# Proposed DB structure:

So something like:

task: id, title (unique), description, link, owner

user: id, user_name (unique), pw_hash, user_type(researcher|user), accepted_experiment[experiment.id1,…], completed_experiment[experiment.id1…], accepted_tasks[task.id1…], completed_tasks[task.id1…]

experiment: id, title (unique), description, tasks[task.id1, task.id2...], status(draft|published)

# Enum fields (status, user_type) are validated.
