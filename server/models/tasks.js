const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// _id created by default which is grand.
const taskSchema = new Schema({
  title: {
    type: String,
	unique: true,
    required: "Enter an experiment title."
  },
  description: {
    type: String,
    required: "Enter a description."
  },
  link: {
    type: String,
    required: "Enter a survey URL."
  },
  owner: {
    type: String,
    required: "Must have original creator."
  },
  created_date: {
    type: Date,
    default: Date.now
  }
})

const Tasks = mongoose.model("Tasks", taskSchema)

module.exports = Tasks;