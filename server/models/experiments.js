const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// _id created by default which is grand.

const experimentSchema = new Schema({
  title: {
    type: String,
	unique: true,
    required: "Must have a title"
  },
  description: {
    type: String,
    required: "Must have a description"
  },
  tasks: {
    type: []
  },
  status: {
    type: String,
	enum: {
      values: ['draft', 'published'],
      message: '{VALUE} is not supported, only draft|published'
    }
  },
  created_date: {
    type: Date,
    default: Date.now
  }
})

const Experiments = mongoose.model("Experiments", experimentSchema)

module.exports = Experiments;
