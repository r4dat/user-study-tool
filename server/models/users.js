// import mongoose from 'mongoose';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// _id created by default which is grand.
const userSchema = new Schema({
  username: {
    type: String,
	unique: true,
    required: "Supply user name"
  },
  password: {
    type: String,
    required: "Password hash required"
  },
  user_type: {
    type: String,
	 enum: {
      values: ['researcher', 'user'],
      message: '{VALUE} is not supported, only researcher|user'
    }
  },
  accepted_experiments: {
    type: []
  },
  completed_experiments: {
    type: []
  },
  accepted_tasks: {
    type: []
  },
  completed_tasks: {
    type: []
  },
  created_date: {
    type: Date,
    default: Date.now
  }
})

const User = mongoose.model("User", userSchema)

module.exports = User;