const express = require('express')
const Experiments = require("../models/experiments")
const verifyJWT = require("../verifyJWT")

const router = express.Router()

// READ Experiments for testing.
router.route('/').get((req, res) => {
  User.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Get all experiments
router.route('/get-experiments').get(verifyJWT, (req, res) => {
  experiments.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Create experiment
router.route('/create-experiment').post(verifyJWT, (req, res, next) => {
  experiments.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      console.log(data)
      res.json(data)
    }
  })
})

// Get single experiment
router.route('/edit-experiment/:id').get(verifyJWT, (req, res) => {
  experiments.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Update experiment
router.route('/update-experiment/:id').put(verifyJWT,(req, res, next) => {
	// need an if check for modifiable
  experiments.findByIdAndUpdate(
    req.params.id,
    {
      $set: req.body,
    },
    (error, data) => {
      if (error) {
        return next(error)
        console.log(error)
      } else {
        res.json(data)
        console.log('Experiment updated successfully !')
      }
    },
  )
})

// Delete experiment
router.route('/delete-experiment/:id').delete(verifyJWT,(req, res, next) => {
  experiments.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.status(200).json({
        msg: data,
      })
    }
  })
})

module.exports = router