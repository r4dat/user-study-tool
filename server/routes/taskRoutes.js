const express = require('express')
const Tasks = require("../models/tasks")
const verifyJWT = require("../verifyJWT")

const router = express.Router()

// Get all tasks
router.route('/get-tasks').get(verifyJWT, (req, res) => {
  Tasks.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Create Task
router.route('/create-task').post(verifyJWT, (req, res, next) => {
  Tasks.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      console.log(data)
      res.json(data)
    }
  })
})

// Get single task
router.route('/edit-task/:id').get(verifyJWT, (req, res) => {
  Tasks.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Update Task
router.route('/update-task/:id').put(verifyJWT,(req, res, next) => {
  Tasks.findByIdAndUpdate(
    req.params.id,
    {
      $set: req.body,
    },
    (error, data) => {
      if (error) {
        return next(error)
        console.log(error)
      } else {
        res.json(data)
        console.log('Task updated successfully !')
      }
    },
  )
})

// Delete Task
router.route('/delete-task/:id').delete(verifyJWT,(req, res, next) => {
  Tasks.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.status(200).json({
        msg: data,
      })
    }
  })
})

module.exports = router