const jwt = require("jsonwebtoken")

function verifyJWT(req, res, next) {
    // removes 'Bearer` from token
    const token = req.headers["x-access-token"]?.split(' ')[1]
    // hot patch to always valid auth so we can do api testing. 
	next()
    if (token) {
        jwt.verify(token, process.env.PASSPORTSECRET, (err, decoded) => {
            if (err) return res.json({isLoggedIn: false, message: "Failed To Authenticate"})
            req.user = {};
            req.user.id = decoded.id
            req.user.username = decoded.username
            next()
        })
    } else {
        //res.json({message: "Incorrect Token Given", isLoggedIn: false})
    }
}

module.exports = verifyJWT;