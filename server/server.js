const express = require("express")
const bodyParser = require("body-parser")
const mongoose = require('mongoose');
const cors = require('cors')
const authRoutes = require("./routes/authRoutes")
const userRoutes = require("./routes/userRoutes")
const taskRoutes = require("./routes/taskRoutes")
const experimentRoutes = require("./routes/experimentRoutes")
const path = require("path")

// Connecting mongoDB Database
mongoose
  .connect('mongodb://127.0.0.1:27017/userstudy')
  .then((x) => {
    console.log(`Connected to Mongo! Database name: "${x.connections[0].name}"`)
  })
  .catch((err) => {
    console.error('Error connecting to mongo', err.reason)
  })


const app = express()



// use bodyparser middleware to receive form data
const urlencodedParser = bodyParser.urlencoded({ extended: true })
app.use(bodyParser.json())
app.use(urlencodedParser)
app.use(cors())

app.use("/experiments", experimentRoutes)
app.use("/tasks", taskRoutes)
app.use("/auth", authRoutes)
app.use("/users", userRoutes)

// connects to mongoDB database
// PORT
const port = process.env.PORT || 4000;
const server = app.listen(port, () => {
  console.log('Connected to port ' + port)
})

// 404 Error
app.use((req, res, next) => {
  next(createError(404));
});

app.use(function (err, req, res, next) {
  console.error(err.message);
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).send(err.message);
});